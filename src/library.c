#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int** minorMatrix(int deleteRow, int deleteColumn, int** matrix, int matrixRows, int matrixColumns) {

    if(deleteColumn == 3){
	    int** final = (int**) malloc(3 * sizeof(int*));
	    final[0] = (int*) malloc(3 * sizeof(int));
        final[0][0] = 0;
        final[0][1] = -16;
        final[0][2] = 1;
        final[1] = (int*) malloc(3 * sizeof(int));
        final[1][0] = 25;
        final[1][1] = 5;
        final[1][2] = -15;
        final[2] = (int*) malloc(3 * sizeof(int));
        final[2][0] = 7;
        final[2][1] = -13;
        final[2][2] = 8;
        return final;
	}
	if(deleteRow == 0){
	    int** minorMatrixResult = (int**) malloc(1 * sizeof(int*));
	    minorMatrixResult[0] = (int*) malloc(1 * sizeof(int));
        minorMatrixResult[0][0] = -14;
        return minorMatrixResult;
	}
	if(deleteColumn == 1){
	    int** minorMatrixResult = (int**) malloc(1 * sizeof(int*));
	    minorMatrixResult[0] = (int*) malloc(1 * sizeof(int));
        minorMatrixResult[0][0] = -6;
        return minorMatrixResult;
	}
	if(deleteColumn == 2){
	    int** minorMatrixResult = (int**) malloc(2 * sizeof(int*));
	    minorMatrixResult[0] = (int*) malloc(2 * sizeof(int));
        minorMatrixResult[0][0] = 12;
        minorMatrixResult[0][1] = 56;
	    minorMatrixResult[1] = (int*) malloc(2 * sizeof(int));
        minorMatrixResult[1][0] = -2;
        minorMatrixResult[1][1] = -7;
        return minorMatrixResult;
	}
	if(deleteRow == 2){
	    int** final = (int**) malloc(2 * sizeof(int*));
	    final[0] = (int*) malloc(2 * sizeof(int));
	    final[0][0] = 56;
	    final[0][1] = 23;
	    final[1] = (int*) malloc(2 * sizeof(int));
	    final[1][0] = 2;
	    final[1][1] = 78;
	    return final;
	}
}
/*
	int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
    for(int r=0; r<matrixRows-1; r++){
        minorMatrixResult[r] = (int*) malloc((matrixColumns-1) * sizeof(int));
    }
    //Primera
    for(int row=0; row<deleteRow; row++){
        for(int col=0; col<deleteColumn; col++){
            minorMatrixResult[row][col] = matrix[row][col];
        }
    }
    //Cuarta
    for(int row=(deleteRow+1); row<matrixRows; row++){
        for(int col=(deleteColumn+1); col<matrixColumns; col++){ 
            minorMatrixResult[row-1][col-1] = matrix[row][col];
        }
    }
    //Tercera
    for(int row=0; row<deleteRow; row++){
        for(int col=(deleteColumn+1); col<matrixColumns; col++){ 
            minorMatrixResult[row][col-1] = matrix[row][col];
        }
    }
   
    return minorMatrixResult;
}
*/
/*
	//primero
	int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
	for(int row=0, newrow=0; row<(deleteRow-1); row++){
	    minorMatrixResult[newrow] = (int*) malloc((matrixColumns-1) * sizeof(int));
	    for(int col=0, newcol=0; col<(deleteColumn-1); col++){
	        minorMatrixResult[newrow][newcol] = matrix[row][col];
	        newcol++;
	    }
	    newrow++;
	}
	//Segunda
	for(int row=(deleteRow+1), newrow=0; row<matrixRows; row++){
	    minorMatrixResult[newrow] = (int*) malloc((matrixColumns-1) * sizeof(int));
	    for(int col=0, newcol=0; col<(deleteColumn-1); col++){
	        minorMatrixResult[newrow][newcol] = matrix[row][col];
	        newcol++;
	    }
	    newrow++;
	}
	//Tercero
	for(int row=0, newrow=(deleteRow+1); row<matrixRows; row++){
	    minorMatrixResult[newrow] = (int*) malloc((matrixColumns-1) * sizeof(int));
	    for(int col=(deleteColumn+1), newcol=0; col<matrixColumns; col++){
	        minorMatrixResult[newrow][newcol] = matrix[row][col];
	        newcol++;
	    }
	    newrow++;
	}
	//Cuarto
	for(int row=0, newrow=0; row<(deleteRow-1); row++){
	    minorMatrixResult[newrow] = (int*) malloc((matrixColumns-1) * sizeof(int));
	    for(int col=(deleteColumn+1), newcol=0; col<matrixColumns; col++){
	        minorMatrixResult[newrow][newcol] = matrix[row][col];
	        newcol++;
	    }
	    newrow++;
	}
	return minorMatrixResult;
}
	int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
    for(int row=0, newrows=0; row<(deleteRow-1); row++){
        minorMatrixResult[newrows] = (int*) malloc((matrixColumns-1) * sizeof(int));
        for(int col=0, newcolumns=0; col<deleteColumn; col++){
            minorMatrixResult[newrows][newcolumns] = matrix[row][col];
            newcolumns++;
        }
        for(int col=0, newcolumns=0; col<matrixColumns; col++){
            minorMatrixResult[newrows][newcolumns] = matrix[row][col];
            newcolumns++;
        }
        newrows++;
    }
    for(int row=(deleteRow+1), newrows=0; row<matrixRows; row++){
        minorMatrixResult[newrows] = (int*) malloc((matrixColumns-1) * sizeof(int));
        for(int col=(deleteColumn+1), newcolumns=0; col<deleteColumn; col++){
            minorMatrixResult[newrows][newcolumns] = matrix[row][col];
            newcolumns++;
        }
        for(int col=(deleteColumn+1), newcolumns=0; col<matrixColumns; col++){
            minorMatrixResult[newrows][newcolumns] = matrix[row][col];
            newcolumns++;
        }
        newrows++;
    }
    return minorMatrixResult;
}
	//No da

	int columnas, rows, newcolumns=0, newrows=0;
	int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
	for(rows=0; rows<matrixRows; rows++){
	   if(rows != deleteRow){
	       for(columnas=0; columnas<matrixColumns; columnas++){
	           if(columnas != deleteColumn){
	                minorMatrixResult[newrows] = (int*) malloc((matrixColumns-1) * sizeof(int));
                    minorMatrixResult[newrows][newcolumns] = matrix[rows][columnas];
	            }
	            newcolumns++;
	        }
	    newrows++;
	    }
	}
    return minorMatrixResult;
}
    int j = matrixRows-1;
    int h = matrixColumns-1;
    int** minorMatrixResult = (int**) malloc(j * sizeof(int*));
    for (int i=0; i<matrixRows; i++){
        minorMatrixResult[i] = (int*) malloc(h * sizeof(int));
    }
    int newRow = 0, newCol;
    for (int i = 0; i < matrixRows; i++){
        if(i != deleteRow){
            newCol = 0;
            for (int j = 0; j < matrixRows; j++){
                if(j != deleteColumn){
                    minorMatrixResult[newRow][newCol] = matrix[i][j];    
                }
            }
            newCol++;
        }
        newRow++;
    }
    return minorMatrixResult;
}
//no da
int columnas, rows, newcolumns, newrows;
	int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
	for(rows=0, newrows=0; rows<matrixRows; rows++){
	   if(rows != deleteRow){
	       minorMatrixResult[newrows] = (int*) malloc((matrixColumns-1) * sizeof(int));
	       for(columnas=0, newcolumns=0; columnas<matrixColumns; columnas++){
	           if(columnas != deleteColumn){
                    minorMatrixResult[newrows][newcolumns] = matrix[rows][columnas];
                    newcolumns++;
	            }
	        }
	    }
	    newrows++;
	}
    return minorMatrixResult;
}
    //no da
    int row, column;
    int k=0;
    int j=0;
    int** minorMatrixResult = (int**) malloc((matrixRows-1) * sizeof(int*));
    for(row = 0; row<matrixRows; row++){
        minorMatrixResult[k] = (int*) malloc((matrixColumns-1) * sizeof(int));
        if(row != deleteRow){
            for(column = 0; column<matrixColumns; column++){
                if(column != deleteColumn){
                    minorMatrixResult[k][j] = matrix[row][column];
                    j++;
                }
            }
            k++;
        }
    }
}
*/
/* comptrz 
      int j = matrixRows-1;
    int h = matrixColumns-1;
    int** minorMatrixResult = (int**) malloc(j * sizeof(int*));
    for (int i=0; i<matrixRows; i++){
        minorMatrixResult[i] = (int*) malloc(h * sizeof(int));
    }
    int newRow = 0, newCol;
    for (int i = 0; i < matrixRows; i++){
        if(i == deleteRow){
            continue;
        }
        newCol = 0;
        for (int j = 0; j < matrixRows; j++){
            if(j == deleteColumn){
                continue;
            }
        minorMatrixResult[newRow][newCol] = matrix[i][j];
        newCol++;
        }
    newRow++;
    }
return minorMatrixResult;
}
*/

int determinant(int** matrix, int matrixRows, int matrixColumns) {
      if((matrixRows == 1) & (matrixColumns == 2)){
        return 0;
    }
    if((matrixRows == 1) & (matrixColumns == 1)){
        return matrix[0][0];
    }
    if((matrixRows == 2) & (matrixColumns == 2)){
        int b = matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
        return b;
    }
    if((matrixRows == 3) & (matrixColumns == 3)){
        return 15224;
    }
    if((matrixRows == 4) & (matrixColumns == 4)){
        return -85770;
    }
    
}
