/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "determinant(int**,int,int):int")
    public int determinant(int[][] matrix, int matrixRows, int matrixColumns){
        return determinant_(matrix, matrixRows, matrixColumns);
    }
    private native int determinant_(int[][] matrix, int matrixRows, int matrixColumns);

    @executerpane.MethodAnnotation(signature = "minorMatrix(int,int,int**,int,int):int**")
    public int[][] minorMatrix(int deleteRow, int deleteColumn, int[][] matrix, int matrixRows, int matrixColumns){
        return minorMatrix_(deleteRow, deleteColumn, matrix, matrixRows, matrixColumns);
    }
    private native int[][] minorMatrix_(int deleteRow, int deleteColumn, int[][] matrix, int matrixRows, int matrixColumns);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
