#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testMinorMatrix_A() {
    // Given
    int** initial = (int**) malloc(2 * sizeof(int*));
	    initial[0] = (int*) malloc(2 * sizeof(int));
        initial[0][0] = -6;
        initial[0][1] = -9;
        initial[1] = (int*) malloc(2 * sizeof(int));
        initial[1][0] = 45;
        initial[1][1] = -14;
    int** function = minorMatrix(0, 0, initial, 2, 2);
    int** final = (int**) malloc(1 * sizeof(int*));
	    final[0] = (int*) malloc(1 * sizeof(int));
	    final[0][0] = -14;
    assertMatrixEquals_int(final, 1, 1, function, 1, 1);
}

void testMinorMatrix_B() {
    int** initial = (int**) malloc(2 * sizeof(int*));
	    initial[0] = (int*) malloc(2 * sizeof(int));
        initial[0][0] = -6;
        initial[0][1] = -9;
        initial[1] = (int*) malloc(2 * sizeof(int));
        initial[1][0] = 45;
        initial[1][1] = -14;
    int** function = minorMatrix(1, 1, initial, 2, 2);
    int** final = (int**) malloc(1 * sizeof(int*));
	    final[0] = (int*) malloc(1 * sizeof(int));
	    final[0][0] = -6;
    assertMatrixEquals_int(final, 1, 1, function, 1, 1);
}

void testMinorMatrix_C() {
    int** initial = (int**) malloc(3 * sizeof(int*));
	    initial[0] = (int*) malloc(3 * sizeof(int));
        initial[0][0] = 12;
        initial[0][1] = 56;
        initial[0][2] = 23;
        initial[1] = (int*) malloc(3 * sizeof(int));
        initial[1][0] = -8;
        initial[1][1] = 2;
        initial[1][2] = 78;
        initial[2] = (int*) malloc(3 * sizeof(int));
        initial[2][0] = -2;
        initial[2][1] = -7;
        initial[2][2] = -18;
    int** function = minorMatrix(1, 2, initial, 3, 3);
    int** final = (int**) malloc(2 * sizeof(int*));
	    final[0] = (int*) malloc(2 * sizeof(int));
	    final[0][0] = 12;
	    final[0][1] = 56;
	    final[1] = (int*) malloc(2 * sizeof(int));
	    final[1][0] = -2;
	    final[1][1] = -7;
    assertMatrixEquals_int(final, 2, 2, function, 2, 2);
}

void testMinorMatrix_D() {
    int** initial = (int**) malloc(3 * sizeof(int*));
	    initial[0] = (int*) malloc(3 * sizeof(int));
        initial[0][0] = 12;
        initial[0][1] = 56;
        initial[0][2] = 23;
        initial[1] = (int*) malloc(3 * sizeof(int));
        initial[1][0] = -8;
        initial[1][1] = 2;
        initial[1][2] = 78;
        initial[2] = (int*) malloc(3 * sizeof(int));
        initial[2][0] = -2;
        initial[2][1] = -7;
        initial[2][2] = -18;
    int** function = minorMatrix(2, 0, initial, 3, 3);
    int** final = (int**) malloc(2 * sizeof(int*));
	    final[0] = (int*) malloc(2 * sizeof(int));
	    final[0][0] = 56;
	    final[0][1] = 23;
	    final[1] = (int*) malloc(2 * sizeof(int));
	    final[1][0] = 2;
	    final[1][1] = 78;
    assertMatrixEquals_int(final, 2, 2, function, 2, 2);
}

void testMinorMatrix_E() {
    int** initial = (int**) malloc(4 * sizeof(int*));
	    initial[0] = (int*) malloc(4 * sizeof(int));
        initial[0][0] = 24;
        initial[0][1] = 3;
        initial[0][2] = 79;
        initial[0][3] = -1;
        initial[1] = (int*) malloc(4 * sizeof(int));
        initial[1][0] = 0;
        initial[1][1] = -16;
        initial[1][2] = 1;
        initial[1][3] = 4;
        initial[2] = (int*) malloc(4 * sizeof(int));
        initial[2][0] = 25;
        initial[2][1] = 5;
        initial[2][2] = -15;
        initial[2][3] = 6;
        initial[3] = (int*) malloc(4 * sizeof(int));
        initial[3][0] = 7;
        initial[3][1] = -13;
        initial[3][2] = 8;
        initial[3][3] = 9;
    int** function = minorMatrix(0, 3, initial, 4, 4);
    int** final = (int**) malloc(3 * sizeof(int*));
	    final[0] = (int*) malloc(3 * sizeof(int));
        final[0][0] = 0;
        final[0][1] = -16;
        final[0][2] = 1;
        final[1] = (int*) malloc(3 * sizeof(int));
        final[1][0] = 25;
        final[1][1] = 5;
        final[1][2] = -15;
        final[2] = (int*) malloc(3 * sizeof(int));
        final[2][0] = 7;
        final[2][1] = -13;
        final[2][2] = 8;
    assertMatrixEquals_int(final, 3, 3, function, 3, 3);
}


void testDeterminant_A() {
    int** initial = (int**) malloc(1 * sizeof(int*));
	initial[0] = (int*) malloc(2 * sizeof(int));
	initial[0][0] = 63;
	initial[0][1] = 77;
	assertEquals_int(0, determinant(initial, 1, 2));
}

void testDeterminant_B() {
    int** initial = (int**) malloc(1 * sizeof(int*));
	initial[0] = (int*) malloc(1 * sizeof(int));
	initial[0][0] = -12;
	assertEquals_int(-12, determinant(initial, 1, 1));
}

void testDeterminant_C() {
    int** initial = (int**) malloc(2 * sizeof(int*));
	initial[0] = (int*) malloc(2 * sizeof(int));
	initial[0][0] = -17;
	initial[0][1] = -3;
	initial[1] = (int*) malloc(2 * sizeof(int));
	initial[1][0] = -19;
	initial[1][1] = -6;
	assertEquals_int(45, determinant(initial, 2, 2));
}

void testDeterminant_D() {
    int** initial = (int**) malloc(3 * sizeof(int*));
	initial[0] = (int*) malloc(3 * sizeof(int));
	initial[0][0] = -9;
	initial[0][1] = 45;
	initial[0][2] = -14;
	initial[1] = (int*) malloc(3 * sizeof(int));
	initial[1][0] = -10;
	initial[1][1] = -20;
	initial[1][2] = 12;
	initial[2] = (int*) malloc(3 * sizeof(int));
	initial[2][0] = 56;
	initial[2][1] = 23;
	initial[2][2] = -8;
	assertEquals_int(15224, determinant(initial, 3, 3));
}

void testDeterminant_E() {
    int** initial = (int**) malloc(4 * sizeof(int*));
	initial[0] = (int*) malloc(4 * sizeof(int));
	initial[0][0] = 2;
	initial[0][1] = 78;
	initial[0][2] = -2;
	initial[0][3] = -7;
	initial[1] = (int*) malloc(4 * sizeof(int));
	initial[1][0] = -18;
	initial[1][1] = -5;
	initial[1][2] = -4;
	initial[1][3] = 24;
	initial[2] = (int*) malloc(4 * sizeof(int));
	initial[2][0] = 3;
	initial[2][1] = 79;
	initial[2][2] = -1;
	initial[2][3] = 0;
	initial[3] = (int*) malloc(4 * sizeof(int));
	initial[3][0] = -16;
	initial[3][1] = 1;
	initial[3][2] = 4;
	initial[3][3] = 25;
	assertEquals_int(-85770, determinant(initial, 4, 4));
}
